import random
# Ferhat Bahceci

def randstring(n):
    return "".join(random.choice(["AT","TA","CG", "GC"]) for i in range(0, n))


def output(t):
    for k in range(0, len(t)):
        s = str(k) + ": "
        if len(t[k]) <= 30:
            for i,e in enumerate(t[k]):
                if i != 0:
                    s = s + ","
                s = s + str(e)
        else:
            s = s + "(" + str(len(t[k])) + " elements)"

        print s

def ht(n):
    return map(lambda x: [], range(n))


def h1(e):
    return (37 * e) % 11

def h2(e):
    return (30 * e) % 8 

def h3(e):
    return (e*e) % 23

def h4(e):
    return sum(map(ord, e)) % 19

def insert(t,h,e):
    
    i = h(e) #gives us which position in hashtable to put the key in
    slot = t.pop(i-1)
    slot.append(e)
    t.insert(i-1,slot)#inserts element e at index i that depends on the used hash-function

    

def search(t,h,e):
    i = h(e) #if the hashfunction h are used, the element e should be stored at index i
    return e in t[i-1]

def delete(t,h,e):
        x = search(t,h,e)
        i = h(e)
        slot = t.pop(i-1)
        slot.remove(e)
        t.insert(i-1,slot)

        return t
    


def test1():
    
    z = map(lambda x: [], range(11))

    for n in range(0,999):
        y = random.randint(0,1000)
        insert(z,h1,y)
        
    output(z)
    


    
def test2():
    
    z = map(lambda x: [], range(8))

    for n in range(0,999):
        y = random.randint(0,1000)
        insert(z,h2,y)
        
    output(z)

def test3():
    
    z = map(lambda x: [], range(23))

    for n in range(0,999):
        y = random.randint(0,1000)
        insert(z,h3,y)
        
    output(z)
def randstring(n):
    return "".join(random.choice(["AT","TA","CG", "GC"]) for i in range(0, n))


def test4():
    
    z = map(lambda x: [], range(11))
    givenlist = [1,12,23,34,45,56,67,78,89,100,111]

    for n in range(len(givenlist)):
        y = givenlist[n]
        insert(z,h1,y)
        
    output(z)
    
def testdna():
    
    z = map(lambda x: [], range(19))
    
    for n in range(999):
        u = randstring(10)
        insert(z,h4,u)

    output(z)
    
    
    
    
    


    

    
    
            
        

    
