
from __future__ import print_function

import os
import array
import sys


# You can implement your sorting algorithm here
def max_heapify(a, i):
    left = 2 * i + 1
    right = 2 * i + 2
    largest = i
    if left < len(a) and a[left] > a[largest]:
        largest = left
    if right < len(a) and a[right] > a[largest]:
        largest = right
    if largest != i:
        a[i], a[largest] = a[largest], a[i]
        max_heapify(a, largest)

def build_max_heap(a):
    for i in range(len(a) // 2, -1, -1):
        max_heapify(a, i)
    
def HeapSort(a):
    B = []
    for n in range (0,len(a)):
        build_max_heap(a)
        B.append(a[0])
        a = a[1:]
        
    B = list(reversed(B))
    a = B
    return a
              




def test():
    # check if nums.txt exists
    if not os.path.exists('nums.txt'):
        print("First create nums.txt")
        sys.exit(0)

    # read the content of nums.txt into an array
    nums = open('nums.txt', 'r')
    a = []
    for line in nums:
        a.append(int(str.strip(line)))


    a = HeapSort(a)
    # output nums_sorted.txt
    nums_sorted = open('nums_sorted.txt', 'w')
    for element in a:
        nums_sorted.write(str(element) + "\n")

    nums.close()
    nums_sorted.close()

    # compare your result (nums_sorted.txt) against the result of the unix/linux sorting algorithm (nums_ref.txt)
    os.system('sort -n nums.txt > nums_ref.txt')
    ret = os.system('diff nums_sorted.txt nums_ref.txt > tmp.txt')

    # Check output differences and output result
    if int(ret) == 0:
        print("Sorted!")

    if int(ret) != 0:
        print("Not sorted!")

# python sort.py runs test
if __name__ == "__main__":
    test()
