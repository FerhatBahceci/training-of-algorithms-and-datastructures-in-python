
from __future__ import print_function

import os
import array
import sys



def insertionSort(a):
   
   for n in range(1,len(a)): #we go through the array from the start to the end of the list

     currentvalue = a[n] #this is the element that are going to be compared
     pos = n #this is the set position that we use so we can check and compare the valuses with the element before

     while pos>0 and a[pos-1]>currentvalue:  #keeps comparing the elements 
         a[pos]=a[pos-1] #swaps the greater element further with the smaller one back to the left of the array a
         pos = pos-1 #we have to decrease pos so we can compare it with the one element to the right of it

     a[pos]=currentvalue  

   return a



def test():
    # check if nums.txt exists
    if not os.path.exists('nums.txt'):
        print("First create nums.txt")
        sys.exit(0)

    # read the content of nums.txt into an array
    nums = open('nums.txt', 'r')
    a = []
    for line in nums:
        a.append(int(str.strip(line)))


    a = insertionSort(a)
    # output nums_sorted.txt
    nums_sorted = open('nums_sorted.txt', 'w')
    for element in a:
        nums_sorted.write(str(element) + "\n")

    nums.close()
    nums_sorted.close()


    os.system('sort -n nums.txt > nums_ref.txt')
    ret = os.system('diff nums_sorted.txt nums_ref.txt > tmp.txt')


    if int(ret) == 0:
        print("Sorted!")

    if int(ret) != 0:
        print("Not sorted!")

# python sort.py runs test
if __name__ == "__main__":
    test()
