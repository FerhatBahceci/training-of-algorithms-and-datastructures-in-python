# This version is for both Python 2.7.x and 3.x.x
# Ferhat Bahceci 


class BSTNode:
    def __init__(self,x,key):
        self.data = x
        self.left = None
        self.right = None
        self.count = 1
        self.parent = key


# Create empty tree
def emptytree ():
    return None

# Insert x into tree t
def insert(t, x):
    if t == None:
        return (x, None , None)
    else:
        key, left, right = t
        if x == key:
            return (key, left, right)
        elif x < key:
            return (key, insert(left, x), right)
        else:
            return (key, left, insert(right, x))

# Create tree by inserting each element in l into an initially empty tree
def treefromlist(l):
    t=emptytree()
    for x in range(0,(len(l))):
        t = insert(t, l[x])
    return t



                           

def inorderwalk(t):
    if t != None:
        key, left, right = t
    else:
        return
    if left == None and right == None:
        if key == None:
            return
        else:
            print key
    else:
        inorderwalk(left)
        print key
        inorderwalk(right)
    return



def findMin(t):
    key, left, right = t
    if left:
        return findMin(left)
    else:
        return t




def delete(t,x):
    if t != None:
       key, left, right = t
    
      
    if key == x:
        if right != None and left!= None:
            temp = findMin(right) #antingen minsta i h�ger halva eller i v�nster, spelar ingen roll
            tempkey, templeft, tempright = temp
            return (tempkey, left, delete(right, tempkey))
        
        elif right!= None and left == None:
            return right
            
        elif right== None and left != None:
            return left

        else:
            return None 
        
    elif x < key:
        return (key,delete(left,x),right)
        
    elif x > key:
        return (key,left,delete(right,x))
        
    return
        
    


    
    
