
from __future__ import print_function

import os
import array
import sys



def quicksort(a):
    less = [] #left subarray, the elements smaller than the pivotelement
    equal = []#the elements that are the same as the pivotelement
    greater = []#right subarray, the elements greater than the pivotelement
    

    if len(a) > 1: #if the array only has the length 1, we know that its already sorted
        pivot = a[0] #sets the first element of the list to the pivot
        for x in a: #goes through the whole array
            if x < pivot: #if the compared element is lesser than the pivot
                less.append(x) #append the lesser element to the subarray less 
            if x == pivot: #if the compared element is the same as the pivot
                equal.append(x)  #append that element to the  subarray equal
            if x > pivot: #if the element is larger than the pivot 
                greater.append(x) #append that element to the subarray greater
       
        return quicksort(less)+equal+quicksort(greater)  # combines all the sorted parts together
  
    else:  
        return a



def test():
    # check if nums.txt exists
    if not os.path.exists('nums.txt'):
        print("First create nums.txt")
        sys.exit(0)

    # read the content of nums.txt into an array
    nums = open('nums.txt', 'r')
    a = []
    for line in nums:
        a.append(int(str.strip(line)))


    a = quicksort(a)
    # output nums_sorted.txt
    nums_sorted = open('nums_sorted.txt', 'w')
    for element in a:
        nums_sorted.write(str(element) + "\n")

    nums.close()
    nums_sorted.close()

    # compare your result (nums_sorted.txt) against the result of the unix/linux sorting algorithm (nums_ref.txt)
    os.system('sort -n nums.txt > nums_ref.txt')
    ret = os.system('diff nums_sorted.txt nums_ref.txt > tmp.txt')

    # Check output differences and output result
    if int(ret) == 0:
        print("Sorted!")

    if int(ret) != 0:
        print("Not sorted!")

# python sort.py runs test
if __name__ == "__main__":
    test()
